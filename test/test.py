#!/usr/bin/env python
import rospy

from std_msgs.msg import String
from legorider_organizer_lib.legorider_organizer import RobotControl

class Robot:
    def __init__(self, robot_name):

        self.linear_speed = 0
        self.rotational_speed = 0
        self.lifter_state = False
        self.gripper_state = False

        robot = RobotControl(robot_name)
        linear_speed = 0
        rotational_speed = 0
        lifter_state = 0
        gripper_state = 0

        key_down_sub = rospy.Subscriber("/keyboard_down", String, self.key_down)
        key_up_sub = rospy.Subscriber("/keyboard_up", String, self.key_up)

        ros_rate = rospy.Rate(10)

        while robot.is_ok():
            robot.control(self.linear_speed, self.rotational_speed)
            robot.gripper(self.gripper_state)
            robot.lifter(self.lifter_state)

            ros_rate.sleep()

    def key_down(self, msg):
        x = 0.2
        r = 0.5
        max_linear_speed = 3.0
        max_rotational_speed = 10.0
        print(msg)

        messages = msg.data.split(',')
        for a in messages:
            print(a)
            if a == "w":
                self.linear_speed += x
                if self.linear_speed > max_linear_speed:
                    self.linear_speed = max_linear_speed
            elif a == "s":
                self.linear_speed -= x
                if self.linear_speed < -max_linear_speed:
                    self.linear_speed = -max_linear_speed
            elif a == "a":
                self.rotational_speed += r
                if self.rotational_speed > max_rotational_speed:
                    self.rotational_speed = max_rotational_speed
            elif a == "d":
                self.rotational_speed -= r
                if self.rotational_speed < -max_rotational_speed:
                    self.rotational_speed = -max_rotational_speed
            elif a == "q":
                self.lifter_state = True
            elif a == "e":
                self.gripper_state = True
            elif a == " ":
                self.linear_speed = 0
                self.rotational_speed = 0

    def key_up(self, msg):
        messages = msg.data.split(',')
        for a in messages:
            if a == "w":
                self.linear_speed = 0
            elif a == "a":
                self.rotational_speed = 0
            elif a == "s":
                self.linear_speed = 0
            elif a == "d":
                self.rotational_speed = 0
            elif a == "q":
                self.lifter_state = False
            elif a == "e":
                self.gripper_state = False
        return

Robot("legorider_organizer")