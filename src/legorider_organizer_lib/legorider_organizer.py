#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64, Bool
from legorider_organizer_description.srv import CameraService, LaserService
from gazebo_msgs.srv import GetModelState

class RobotControl:

    def __init__(self, robot_name):

        self.robot_name = robot_name
        self.model_names = ['lego_block_1','lego_block_2','lego_block_3','lego_block_4','lego_block_5']

        rospy.init_node(self.robot_name + '_controller')

        self.contact_pub = rospy.Publisher('/' + self.robot_name + '/contact', Bool, queue_size=1)
        self.contact_msg = Bool()

        self.left_motor_pub = rospy.Publisher('/' + self.robot_name + '/wheel_left_joint/set_speed', Float64, queue_size=10)
        self.right_motor_pub = rospy.Publisher('/' + self.robot_name + '/wheel_right_joint/set_speed', Float64, queue_size=10)
        self.rear_left_motor_pub = rospy.Publisher('/' + self.robot_name + '/wheel_rear_left_joint/set_speed', Float64, queue_size=10)
        self.rear_right_motor_pub = rospy.Publisher('/' + self.robot_name + '/wheel_rear_right_joint/set_speed', Float64, queue_size=10)
        self.lifter_pub = rospy.Publisher('/' + self.robot_name + '/lifter_joint/set_speed', Float64, queue_size=10)
        self.grabber_left_pub = rospy.Publisher('/' + self.robot_name + '/grabber_left_joint/set_speed', Float64, queue_size=10)
        self.grabber_right_pub = rospy.Publisher('/' + self.robot_name + '/grabber_right_joint/set_speed', Float64, queue_size=10)

        self.left_motor_msg = Float64()
        self.right_motor_msg = Float64()
        self.lifter_msg = Float64()
        self.left_gripper_msg = Float64()
        self.right_gripper_msg = Float64()

        self.image = self._Image()

        self.init_sensor_services()

    def init_sensor_services (self):

        try:
            
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.get_model_state = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

            self.image_data_service = rospy.ServiceProxy('/legorider_organizer/get_image', CameraService)
            self.front_left_service = rospy.ServiceProxy('/legorider_organizer/get_front_left_range', LaserService)
            self.front_right_service = rospy.ServiceProxy('/legorider_organizer/get_front_right_range', LaserService)
            self.left_service = rospy.ServiceProxy('/legorider_organizer/get_left_range', LaserService)
            self.right_service = rospy.ServiceProxy('/legorider_organizer/get_right_range', LaserService)
            self.cross_left_service = rospy.ServiceProxy('/legorider_organizer/get_cross_left_range', LaserService)
            self.cross_right_service = rospy.ServiceProxy('/legorider_organizer/get_cross_right_range', LaserService)

            resp = self.image_data_service()

            self.image.height = resp.height
            self.image.width = resp.width

            return True

        except (rospy.ServiceException, rospy.ROSException):
            print("no camera sensor")
            self.status = "stop"
            return False
    
    def gripper(self, state):
        if state == True:
            self.left_gripper_msg.data = 0.05
            self.right_gripper_msg.data = -0.05
        else:
            self.left_gripper_msg.data = -0.05
            self.right_gripper_msg.data = 0.05

        self.grabber_left_pub.publish(self.left_gripper_msg)
        self.grabber_right_pub.publish(self.right_gripper_msg)

    def lifter(self, state):
        if state == True:
            self.lifter_msg.data = -0.05
        else:
            self.lifter_msg.data = 0.05

        self.lifter_pub.publish(self.lifter_msg)

    def control(self, linear, rotation):
        wheel_separation = 0.5

        self.right_motor_msg.data = linear + rotation * wheel_separation / 2.0
        self.left_motor_msg.data = linear - rotation * wheel_separation / 2.0

        self.left_motor_pub.publish(self.left_motor_msg)
        self.right_motor_pub.publish(self.right_motor_msg)
        self.rear_left_motor_pub.publish(self.left_motor_msg)
        self.rear_right_motor_pub.publish(self.right_motor_msg)

    def get_sensor_data(self):
        image = self.image_data()
        sensor_row = []
        for i in range(image.width):
            brightness = (0.2126 * ord(image.data[i * 3])) + (0.7152 * ord(image.data[i * 3 + 1])) + (0.0722 * ord(image.data[i * 3 + 2]))
            sensor_row.append(brightness)
        return sensor_row

    def front_left_laser_data(self):
        resp = self.front_left_service()

        return resp.data

    def front_right_laser_data(self):
        resp = self.front_right_service()

        return resp.data

    def right_laser_data(self):
        resp = self.right_service()

        return resp.data

    def left_laser_data(self):
        resp = self.left_service()

        return resp.data

    def cross_left_laser_data(self):
        resp = self.cross_left_service()

        return resp.data

    def cross_right_laser_data(self):
        resp = self.cross_right_service()

        return resp.data

    def image_data(self):
        resp = self.image_data_service()

        #self.image.data = resp.data
        self.image.data = []

        for pixel in resp.data:
            self.image.data.append(ord(pixel))

        return self.image

    def is_ok(self):
        if not rospy.is_shutdown():
            #self.image_data()
            return True
        else:
            return False
    
    class _Image:
        def __init__(self):
            self.height = 0
            self.width = 0
            self.data = []
